#!/usr/bin/env python

import argparse
import json

from functions import find_sequences


class AwesomeEncoder (json.JSONEncoder):
	def default (self, obj):
		if isinstance(obj, set):
			return list(obj)
		return json.JSONEncoder.default(self, obj)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		formatter_class=argparse.RawDescriptionHelpFormatter,
		description='Search for patterns in a file',
		epilog="""
This is consile interface for sequence analyzer,
it will output a valid JSON dictionary
with pattern names as keys and lists of match tuples as values.
\nEncoding is not required for hex pattern matching."""
	)
	parser.add_argument('file_path', metavar='file', type=str, help='Path to a file that will be analyzed')
	parser.add_argument('--map', metavar='file', type=str, help='Path to a pattern map file (default: default.map)')
	parser.add_argument('--encoding', metavar='encoding', type=str, help='Pattern map encoding (default: utf-8)')
	parser.add_argument('--chunk', metavar='bytes', type=int, help='Chunk size')
	parser.add_argument('--overlap', metavar='bytes', type=int, help='Chunk overlap size')
	parser.add_argument('--threads', metavar='N', type=int, help='How many processes to spawn')
	parser.add_argument('--indent', metavar='string', type=int, help='Number of spaces for indentation, -1 for tabs')
	args = parser.parse_args()

	find_kwargs = {'path': args.file_path }

	if args.map:
		from functions import load_map
		map_kwargs = {'path': args.map}
		if args.encoding: map_kwargs['encoding'] = args.encoding
		pattern_map = load_map(**map_kwargs)
		find_kwargs['pattern_map'] = pattern_map

	if args.chunk:
		find_kwargs['chunk_size'] = args.chunk
	if args.overlap:
		find_kwargs['tail_chunk_size'] = args.overlap
	if args.threads:
		find_kwargs['threads'] = args.threads

	data = find_sequences(**find_kwargs)
	indent = '\t' if args.indent == -1 else args.indent
	print(json.dumps( data, cls=AwesomeEncoder, sort_keys=True, indent=indent ))
