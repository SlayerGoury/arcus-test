### Command line interface

Execute `cli.py [--map file] [--encoding encoding] [--chunk bytes] [--overlap bytes] [--threads N] [--indent string] file`  
to get a JSON of matches.  
Its schema will look something like this:  
```
{
	pattern_name_1: [
		[match_start, match_end],
		[match_start, match_end],
		[match_start, match_end],
	]
	pattern_name_2: [
		[match_start, match_end],
		[match_start, match_end],
		[match_start, match_end],
	]
}
```

### Python module

To use this thing inside another python script, do this:  
`from functions import find_sequences`  
and then call `find_sequences(path, **options)`

Options for \*\*options:  
`chunk_size`: amount of bytes per slice of file to read, default is 1 megabyte  
`tail_chunk_size`: extra amount of bytes to overlap chunks, because some patterns may happen to exist across chunks, default is 4 kilobytes  
`threads`: number of threads (processes, python-speaking) to spawn, default is os.cpu_count()  
`quiet`: do not output progress, default is True  
`profile`: output total time, default is False

To load non-derault map `from functions import load_map`  
and then `pattern_map = load_map(path, encoding)` and pass it as `pattern_map` argument into `find_sequences`

Example:  
```
from functions import find_sequences,  load_map
pattern_map = load_map('default.map')
result = find_sequences('big_files/bonito-qq2a.200305.002-factory-418015e0.zip', profile=True, quiet=False, pattern_map=pattern_map)
```

### Testing

Place a file named `test_data.zip` into working directory so every test will perform actual test.  
Execute `tests.py`.  
Wait for tests to complete.  
If the last output like says anyting other than `OK`, evalueate what have failed and proceed accordingly.
