import binascii
import json
import os
import re

from concurrent.futures import ProcessPoolExecutor, as_completed


class Options:
	"""
	Instantiate an object containing default options that can be overriden on initialization.
	Could be done much easier, but I like this way for no particular reason.
	"""

	def __init__ (self, **kwargs):
		self.o = {
			'tail_chunk_size': 1024*4,
			'chunk_size': 1024*1024*1,
			'threads': os.cpu_count(),
			'quiet': True,
			'profile': False,
		}
		for key in self.o.keys():
			value = kwargs.get(key)
			if value is not None:
				self.o[key] = value

	def __getattr__ (self, attr):
		return self.o.get(attr)


def escape_hex_pattern (pattern, encoding):
	"""
	Escapes pattern if it is a hex, otherwise returns it as it is
	"""

	try:
		pattern = re.escape( bytes.fromhex(pattern) )
	except ValueError:
		pattern = re.escape( bytes(pattern, encoding) )

	return pattern


def load_map (path='default.map', encoding='utf-8'):
	"""
	Map file loader.
	Map is expected to be a valid JSON dictionary with both keys and values being strings.
	Returns dict with excaped patterns as keys and descriptions as values.
	"""

	with open(path) as map_file:
		map_dict = json.load(map_file)

	assert type(map_dict) is dict
	for key, value in map_dict.items():
		assert type(key) is str
		assert type(value) is str

	return { escape_hex_pattern(pattern, encoding): pattern_type for pattern, pattern_type in map_dict.items() }


def compile_patterns (pattern_map):
	"""
	Search pattern precompilator.
	Does not actually make noticeable change, but it is supposed to be better this way.
	"""

	return [ (re.compile(pattern), pattern_type) for pattern, pattern_type in pattern_map.items() ]


def search (data, patterns):
	"""
	Searches for patterns inclusion in a chunk of data.
	"""

	return {
		pattern_type: [ match for match in pattern.finditer(data) ]
		for pattern, pattern_type in patterns
	}


def process_matches (matches, offset):
	"""
	Processes re.Match objects into offset tuples.
	Because you can not return re.Match from a subprocess.
	"""

	return {
		key: [ (match.start()+offset, match.end()+offset) for match in matches[key] ]
		for key in matches.keys()
	}


def process_chunk (path, offset, pattern_map, chunk_size, tail_chunk_size, quiet ):
	"""
	Data chunk processor.
	Give it data and it will return a dict of matches per key.
	It will read a chunks of a file near given offset and call for processing.
	Tail chunk is a workaround for patterns being split between chunks.

	This is what goes into each subprocess.
	"""

	if not quiet:
		print( '\rProcessing chunk at {}'.format(offset), end="" )
	if tail_chunk_size > offset:
		chunk_size = chunk_size + tail_chunk_size
	else:
		offset -= tail_chunk_size
		chunk_size = chunk_size + 2*tail_chunk_size

	with open(path, 'rb') as big_data_file:
		big_data_file.seek(offset)
		chunk = big_data_file.read(chunk_size)

	patterns = compile_patterns(pattern_map)
	matches = search(chunk, patterns)
	result = process_matches(matches, offset)
	return result


def find_sequences (path, pattern_map=load_map(), **kwargs):
	"""
	Start unraveling from here.
	This one spawns process_chunk processes and joins results into results dict.
	"""

	options = Options(**kwargs)
	pool = []
	results = { value:set() for value in pattern_map.values() }
	offset = 0
	file_size = os.path.getsize(path)

	if options.profile:
		import time
		t_start = time.time()

	with ProcessPoolExecutor(max_workers=options.threads) as executor:
		while offset < file_size:
			pool.append( executor.submit(process_chunk, path, offset, pattern_map, options.chunk_size, options.tail_chunk_size, options.quiet) )
			offset += options.chunk_size+options.tail_chunk_size
		for process in as_completed(pool):
			data = process.result()
			for key in data.keys():
				for match in data[key]:
					results[key].add(match)

	if options.profile:
		print( '\nIt took {} seconds to process it in {}MB chunks'.format(
			round( time.time()-t_start, 2 ),
			round( (options.tail_chunk_size+options.chunk_size)/1024/1024, 2)
		))

	return results
