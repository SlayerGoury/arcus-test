#!/usr/bin/env python

import os
import re
import unittest

from typing.re import Pattern

from functions import *

HOW_MUCH_DATA_TO_READ = 1024*1024*100


class Tests (unittest.TestCase):

	def setUp (self):
		if not os.path.exists('test_data.zip'):
			print('Place a file named test_data.zip in this directory for better testing')
			self.data = b''
		else:
			with open('test_data.zip', 'rb') as file:
				self.data = file.read(HOW_MUCH_DATA_TO_READ)

	def test_options (self):
		options = Options( quiet = 'meow', profile= 'purr' )                                                  # Options.__init__ should accept kwargs
		self.assertEqual ( options.quiet, 'meow' )                                                            # Options.__getattr__ should return stored vale
		self.assertEqual ( options.profile, 'purr' )                                                          # Options.__getattr__ should return stored vale

	def test_escape_hex_pattern (self):
		self.assertEqual( escape_hex_pattern('AABBCCDDEEFF', ''), b'\\\xaa\\\xbb\\\xcc\\\xdd\\\xee\\\xff' )   # Hex pattern escaped
		self.assertEqual( escape_hex_pattern('me.{1-10}ow', 'utf-8' ), b'me\\.\\{1\\-10\\}ow' )               # Non-hex pattern escaped

	def test_load_map (self):
		pattern_map = load_map()

		# assert schema is correct
		self.assertIs( type(pattern_map), dict )
		for key, value in pattern_map.items():
			self.assertIs( type(key), bytes )
			self.assertIs( type(value), str )

	def test_compile_patterns (self):
		pattern_map = load_map()
		compiled_patterns = compile_patterns(pattern_map)

		# assert schema is correct
		self.assertIs( type(compiled_patterns), list )
		for item in compiled_patterns:
			self.assertIs( type(item), tuple )
			self.assertTrue( isinstance(item[0], Pattern) )
			self.assertIs( type(item[1]), str )

	def test_search (self):
		pattern_map = load_map()
		patterns = compile_patterns( pattern_map )
		result = search(self.data, patterns)

		# assert schema is correct
		self.assertIs( type(result), dict )
		for key, value in result.items():
			self.assertIn( key, pattern_map.values() )                                                           # also test that keys are what is in patterns
			self.assertIs( type(value), list )
			for item in value:
				self.assertIs( type(item), type(re.match('', '')) )

	def test_process_matches (self):
		pattern_map = load_map()
		patterns = compile_patterns( pattern_map )
		result = process_matches( search(self.data, patterns), 0 )

		# assert schema is correct
		self.assertIs( type(result), dict )
		for key, value in result.items():
			self.assertIn( key, pattern_map.values() )                                                           # also test that keys are what is in patterns
			self.assertIs( type(value), list )
			for item in value:
				self.assertIs( type(item), tuple )
				self.assertEqual( len(item), 2 )
				self.assertIs( type(item[0]), int )
				self.assertIs( type(item[1]), int )

	def test_process_chunk (self):
		if not os.path.exists('test_data.zip'):
			raise ValueError('File test_data.zip was not found in current directory')
		pattern_map = load_map()
		result = process_chunk('test_data.zip', 0, pattern_map, HOW_MUCH_DATA_TO_READ, 1024, True)

		# assert schema is correct
		self.assertIs( type(result), dict )
		for key, value in result.items():
			self.assertIn( key, pattern_map.values() )                                                           # also test that keys are what is in patterns
			self.assertIs( type(value), list )
			for item in value:
				self.assertIs( type(item), tuple )
				self.assertEqual( len(item), 2 )
				self.assertIs( type(item[0]), int )
				self.assertIs( type(item[1]), int )

	def test_find_sequences (self):
		result = find_sequences('test_data.zip')

		# assert schema is correct
		self.assertIs( type(result), dict )
		for key, value in result.items():
			self.assertIs( type(key), str )
			self.assertIs( type(value), set )
			for item in value:
				self.assertIs( type(item), tuple )
				self.assertEqual( len(item), 2 )
				self.assertIs( type(item[0]), int )
				self.assertIs( type(item[1]), int )


if __name__ == "__main__":
	unittest.main()
